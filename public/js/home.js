var criterios = {};


(function () {
    $(document).ready(function () {
       
        $('select[name=status]').on('change', function(){
           
            $("#tabela_atividades").loading({
                theme: 'light',
                message: 'Carregando...'
            });
           
           var campoStatus = $(this);
           criterios.status = campoStatus.val();
           $.ajax({
              type: "POST",
              url: "/api/buscar",
              data: criterios,
              success: tratarSucesso,
              error:  tratarErro,
              dataType: "json"
           });
           
       });
       
       $('select[name=situacao]').on('change', function(){
           $("#tabela_atividades").loading({
               theme: 'light',
               message: 'Carregando...'
           });
           
           var campoSituacao = $(this);
           criterios.situacao = campoSituacao.val();
           $.ajax({
              type: "POST",
              url: "/api/buscar",
              data: criterios,
              success: tratarSucesso,
              error:  tratarErro,
              dataType: "json"
           });
           
       });
       
       function tratarErro(dados) {
           $("#tabela_atividades").loading('stop');
           alert("Falha ao obter os dados");
           
       }
       
       function tratarSucesso(dados) {
           atividades = dados.data;
           var tabelaAtividades = $("#tabela_atividades");
           var corpoTabelaAtividades = tabelaAtividades.find("tbody");
           var lista_paginacao = $("#lista_paginacao");
           var containerTabela = tabelaAtividades.parent();
           
           var mensagem = $('#mensagem') 
           
           
           if(mensagem.length === 0) {
                mensagem = $("<div id = 'mensagem' class = 'alert alert-warning'> <i class = 'fa fa-info' > </i> Nenhuma Atividade Encontrada.</div>");
                containerTabela.append(mensagem);
           }
           
           if(atividades.length === 0) {
               tabelaAtividades.find('thead,tbody').hide();
               mensagem.show();
           }
           else {
               tabelaAtividades.find('thead,tbody').show();
               mensagem.hide();
           }
               
           
           corpoTabelaAtividades.find('tr').remove();
           lista_paginacao.find('li').remove();
           
           for(var a = 0; a < atividades.length; a++) {
               var atividade = atividades[a];
               var linha = montarLinha(atividade);
               corpoTabelaAtividades.append(linha);
           }
           
           $("#tabela_atividades").loading('stop');
           
       }
       
       function montarLinha(atividade) {
           var linha = $('<tr></tr>');
           linha.addClass('text-black-50');
           if(atividade.status.id === 4) {
               linha.addClass('table-success');
           }
           var th = $("<th scope = 'row' >"+ atividade.id +"</th>");
           var celulaNome = $("<td>"+ atividade.nome +"</td>");
           var celulaStatus = null;
           
           
           switch(atividade.status.id) {
               case 1:
                   celulaStatus = $("<td><span class = 'badge badge-danger'>"+ atividade.status.descricao +"</span></td>");
               break;
               case 2:
                   celulaStatus = $("<td><span class = 'badge badge-warning'>"+ atividade.status.descricao +"</span></td>");
               break;
               case 3:
                   celulaStatus = $("<td><span class = 'badge badge-info'>"+ atividade.status.descricao +"</span></td>");
               break;
               case 4:
                   celulaStatus = $("<td><span class = 'badge badge-success'>"+ atividade.status.descricao +"</span></td>");
               break;
           }
           
           
           var celulaSituacao = null;
           switch(atividade.situacao.id) {
               case 1:
                   celulaSituacao = $("<td><span class = 'badge badge-primary'>"+ atividade.situacao.descricao +"</span></td>");
               break;
               case 2:
                   celulaSituacao = $("<td><span class = 'badge badge-dark'>"+ atividade.situacao.descricao +"</span></td>");
               break;
               
           }
           
           var stringAcao = "<td><a class='btn btn-info text-white mr-1' title='Ver Detalhes' href='/ver-detalhes/"+ atividade.uuid +"'><i class='fa fa-search'></i><a class='btn btn-info text-white' title='Editar Atividade' href='/alterar-atividade/"+ atividade.uuid +"'>  <i class='fa fa-edit'></i></a></td>";
           var celulaAcao = $(stringAcao);
           linha.append(th);
           linha.append(celulaNome);
           linha.append(celulaStatus);
           linha.append(celulaSituacao);
           linha.append(celulaAcao);
           return linha;
       }
       
       
    });
})();





