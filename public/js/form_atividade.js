(function () {
    $(document).ready(function () {
        $('#inicio_em').mask('99/99/9999');
        $('#termino_em').mask('99/99/9999');

        $("#inicio_em").datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: "1920:2100"
        });
        $("#inicio_em").datepicker("option", "dateFormat", "dd/mm/yy");
        $("#inicio_em").datepicker("option", $.datepicker.regional["pt-BR"]);

        $("#termino_em").datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: "1920:2100"
        });
        $("#termino_em").datepicker("option", "dateFormat", "dd/mm/yy");
        $("#termino_em").datepicker("option", $.datepicker.regional["pt-BR"]);

        $('#descricao').keypress(function () {
            var length = $(this).val().length - 1;
            var maxlength = $(this).attr('maxlength') - 1;
            if (length >= maxlength) {
                $(this).val($(this).val().substring(length - maxlength, length));
            }
        });
        
        $("#form_atividade").on('submit', function(){
           $("#btn_submit").attr('disabled', true); 
        });

    });

})();
