<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/nova-atividade', 'HomeController@abrirFormularioNovaAtividade')
      ->name('nova-atividade');



Route::get('/alterar-atividade/{uuid}', 'HomeController@abrirFormularioAlteracao')
      ->name('alterar-atividade');


Route::get('/ver-detalhes/{uuid}', 'HomeController@abrirDetalhes')
      ->name('ver-detalhes');


Route::post('/cadastrar-atividade', 'HomeController@cadastrarAtividade')
      ->name('cadastrar-atividade');

Route::post('/editar-atividade', 'HomeController@editarAtividade')
      ->name('editar-atividade');

