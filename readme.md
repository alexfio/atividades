# Aplicação Para Controle de Atividades
**Francisco Chagas Alexandre de Souza Filho** - alexfiofcasf@gmail.com

# Informações Preliminares
- O presente código é uma implementação de um controle de atividades.
- Como SGBD relacional foi usado o MySQL, no front-end Bootstrap 4.0 e JQuery. Além disso
foi usado PHP 7.2 e Laravel. 
- O modelo de dados, **modelo.png**, encontra-se na pasta src;
- O dump do banco de dados, **atividades.sql**, encontra-se na pasta src;
- **Atenção**: Nas instruções de instalação, logo abaixo, há a indicação de como rodar as migrations do Laravel
  para construir o esquema do banco de dados e adicionar os dados das tabelas auxiliares,
  "status" e "situacoes". Caso deseje instalar o projeto sem usar o Docker e rodar o script atividades.sql ao invés das migrations para construir o esquema do banco de dados,
  desconsidere os passos **Rodar as migrations e os seeders** nas instruções abaixo, na opção de instalação sem Docker.

**Requisitos Funcionais :**

 - Tela de Listagem:

    - Tela para listar as atividades, podendo filtrar pelo “Status” e pela “Situação”;

    - A cada atividade listada, deve-se checar o “Status” dela e se for “Concluído”, alterar a cor de fundo da linha;

    - Na linha de cada atividade listada, exibir um botão de “Editar” para acessar a tela de
edição da atividade;

    - No final da tela, exibir um botão para incluir uma nova atividade.

 - Tela de Cadastro/Atualização:
    
    - Tela para fazer a manutenção das atividades, podendo alterar os campos disponíveis,
respeitando as regras citadas a seguir.
 


# Instruções de Instalação

- Para uma maior facilidade de execução do projeto, o arquivo **.env**
foi adicionado ao git. Esse arquivo já está preparado para o uso com o Docker, **caso queira executar o sistema sem usar o Docker,
altere as configurações do arquivo para os valores adequados, principalmente o endereço de Host do Mysql, bem como
usuário e senha.**
         

O projeto pode ser executado com Docker ou sem o Docker.

   - **Com o Docker**:

       **Certifique-se de que as portas 80 e 3306 estão livres.**

       -  Na raiz do projeto execute:
           - docker-compose up -d
       - Instalar as dependências do php via composer: 
           - docker exec -it web composer install
       - Instalar as dependências de front-end via bower:
           - docker exec -it web bower --allow-root install
       - Rodar as migrations e os seeders:
           - docker exec -it web php artisan **migrate** **--seed** 
      - No navegador digite: **localhost** para acessar o projeto .
      - Para destruir os containers após a execução da aplicação faça: 
        docker-compose down
	  
- **Sem o Docker**:
    - É necessário que os seguintes softwares estejam instalados:
        - **Composer**, **Bower**, **PHP 7.2**, **MySQL 5.7**.

    - No mysql **crie um novo banco de dados** chamado atividades    

    - No arquivo **.env**, na raiz do projeto, altere os parâmetros de conexão com o MySQL para os valores adequados.

    - No MySQL, com o banco de dados atividades selecionado, rode o script atividades.sql. (**Caso não queira criar o esquema do banco de dados usando migrations**)
                   
    - Na raiz do projero execute
	     -  composer install
	     -  bower  install
	     -  Rodar as migrations e os seeders (**Caso não tenha executado o script atividades.sql**):
		   	- php artisan **migrate** **--seed** 
	     -  php artisan serve
	- No navegador digite: **localhost:8000** para acessar o projeto .
		   
	 
