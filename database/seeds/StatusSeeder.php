<?php

use Illuminate\Database\Seeder;
use Atividades\Persistence\Eloquent\Model\Status;
use Ramsey\Uuid\Uuid;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $s = new Status();
        $s->uuid = Uuid::uuid4()->toString();
        $s->descricao = "Pendente";
        $s->save();
        
        $s = new Status();
        $s->uuid = Uuid::uuid4()->toString();
        $s->descricao = "Em Desenvolvimento";
        $s->save();
        
        $s = new Status();
        $s->uuid = Uuid::uuid4()->toString();
        $s->descricao = "Em Teste";
        $s->save();
        
        $s = new Status();
        $s->uuid = Uuid::uuid4()->toString();
        $s->descricao = "Concluído";
        $s->save();
        
        
    }
}
