<?php

use Illuminate\Database\Seeder;
use Ramsey\Uuid\Uuid;
use Atividades\Persistence\Eloquent\Model\Situacao;

class SituacoesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $s = new Situacao();
        $s->uuid = Uuid::uuid4()->toString();
        $s->descricao = "Ativo";
        $s->save();
        
        $s = new Situacao();
        $s->uuid = Uuid::uuid4()->toString();
        $s->descricao = "Inativo";
        $s->save();
        
    }
}
