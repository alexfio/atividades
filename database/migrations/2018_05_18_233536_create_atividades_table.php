<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtividadesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
       
        Schema::create('atividades', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->unique();
            $table->string('nome', 255);
            $table->string('descricao', 600);
            $table->date('data_inicio');
            $table->date('data_fim')->nullable();
            $table->integer('status_id')->unsigned();
            $table->integer('situacao_id')->unsigned();
        });

        Schema::table('atividades', function (Blueprint $table) {
            $table->foreign('status_id')
                    ->references('id')
                    ->on('status')
                    ->onUpdate('cascade')
                    ->onDelete('restrict');

            $table->foreign('situacao_id')
                    ->references('id')
                    ->on('situacoes')
                    ->onUpdate('cascade')
                    ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        
        Schema::table('atividades', function (Blueprint $table) {
            $table->dropForeign(['situacao_id']);
            $table->dropForeign(['status_id']);
            $table->dropUnique(['uuid']);
            
        });
        
        Schema::dropIfExists('atividades');
    }

}
