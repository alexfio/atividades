<?php

namespace Atividades\Http\Controllers;

use Illuminate\Http\Request;
use Atividades\Http\Requests\AtividadeRequest;
use Atividades\Persistence\Repositories\AtividadesRepositories;

class HomeController extends Controller
{
    private $atividadesRepository;

    public function __construct(AtividadesRepositories $atividadesRepository)
    {
        $this->atividadesRepository = $atividadesRepository;
    }

    public function index()
    {
        $dados['atividades'] = $this->atividadesRepository->getAll();
        return view('home', $dados);
    }

    public function abrirFormularioNovaAtividade()
    {
        return view('nova_atividade');
    }
    
    public function abrirDetalhes(string $uuid)
    {
        $atividade = $this->atividadesRepository->getByUuid($uuid);
        $dados['atividade'] = $atividade;
        $dados['atividade']['data_inicio'] = date('d/m/Y', strtotime($atividade['data_inicio']));
        $dados['atividade']['data_fim'] = isset($dados['atividade']['data_fim']) ? date('d/m/Y', strtotime($atividade['data_fim'])) : '';
        return view('ver-detalhes', $dados);
    }
    
    public function abrirFormularioAlteracao(Request $request, $uuid)
    {
        $atividade = $this->atividadesRepository->getByUuid($uuid);
        $dados['atividade'] = $atividade;
        $dados['atividade']['data_inicio'] = date('d/m/Y', strtotime($atividade['data_inicio']));
        $dados['atividade']['data_fim'] = isset($dados['atividade']['data_fim']) ? date('d/m/Y', strtotime($atividade['data_fim'])) : '';
        
        if ($atividade['status']['id'] == 4) {
            $msg_conclusao = "Esta atividade foi concluída e não pode ser alterada";
            $request->session()->flash("msg_conclusao", $msg_conclusao);
            return view('ver-detalhes', $dados);
        }
        
        return view('editar_atividade', $dados);
    }

    public function cadastrarAtividade(AtividadeRequest $request)
    {
        try {
            $dados = $request->except('_token');
            $dados['inicio'] = \DateTime::createFromFormat('d/m/Y', $dados['inicio']);
            $dados['termino'] = !empty($dados['termino']) ? \DateTime::createFromFormat('d/m/Y', $dados['termino']) : null;

            $this->atividadesRepository->save($dados);
            
            $mensagem = "Atividade Cadastrada com Sucesso !";
            $request->session()->flash('cadastrado_sucesso', $mensagem);
            
            return redirect()
                    ->action("HomeController@index");
        } catch (\Exception $ex) {
            $mensagem = "Falha: " . $ex->getMessage();
            $request->session()->flash('cadastrado_falha', $mensagem);
            
            return redirect()->back()->withInput();
        }
    }
    
    public function editarAtividade(AtividadeRequest $request)
    {
        try {
            $dados = $request->except('_token');
            $dados['inicio'] = \DateTime::createFromFormat('d/m/Y', $dados['inicio']);
            
            $dados['termino'] = !empty($dados['termino']) ?
                    \DateTime::createFromFormat('d/m/Y', $dados['termino']) : null;

            $this->atividadesRepository->update($dados);
            
            $mensagem = "Atividade Atualizada com Sucesso !";
            $request->session()->flash('cadastrado_sucesso', $mensagem);
            
            return redirect()
                    ->action("HomeController@index");
        } catch (\Exception $ex) {
            $mensagem = "Falha: " . $ex->getMessage();
            $request->session()->flash('cadastrado_falha', $mensagem);
            
            return redirect()->back()->withInput();
        }
    }
    
    public function buscar(Request $request)
    {
        $criterios = $request->all();
        return $this->atividadesRepository->getByCriteria($criterios);
    }
}
