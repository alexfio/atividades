<?php

namespace Atividades\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use voku\helper\AntiXSS;

class AtividadeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function prepareForValidation()
    {
        $antiXSS = new AntiXSS();
        $dados = $this->except('_token');
        
        //Higienizando os dados
        foreach ($dados as $index => $valor) {
            $dados[$index] = \strip_tags($valor);
            $dados[$index] = $antiXSS->xss_clean($dados[$index]);
            $dados[$index] = \preg_replace('/\s{2,}/', ' ', $dados[$index]);
        }
        
        $this->replace($dados);
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "nome" => "required",
            "inicio" => "required|date_format:d/m/Y",
            "termino" => "required_if:status,4|date_format:d/m/Y|after_or_equal:inicio",
            "status" => "required",
            "situacao" => "required",
            "descricao" => "required|max:600"
        ];
    }
    
    public function messages()
    {
        return [
            "inicio.required" => "O campo início é obrigatório.",
            "inicio.date_format" => "O campo início não corresponde ao formato dd/mm/aaaa.",
            "termino.date_format" => "O campo término não corresponde ao formato dd/mm/aaaa.",
            "termino.after_or_equal" => "O campo término deve ser uma data posterior ou igual a início.",
            "termino.required_if" => "O campo término é obrigatório quando o campo status for \"Concluído\".",
            "situacao.required" => "O campo situação é obrigatório.",
            "descricao.required" => "O campo descrição é obrigatório.",
            "descricao.max" => "O campo descrição deve possuir no máximo 600 caracteres."
        ];
    }
}
