<?php

namespace Atividades\Providers;

use Illuminate\Support\ServiceProvider;
use Atividades\Persistence\Repositories\SituacoesRepository;
use Atividades\Persistence\Eloquent\Repositories\SituacoesEloquentRepository;
use Atividades\Persistence\Repositories\StatusRepository;
use Atividades\Persistence\Eloquent\Repositories\StatusEloquentRepository;
use Atividades\Persistence\Repositories\AtividadesRepositories;
use Atividades\Persistence\Eloquent\Repositories\AtividadesEloquentRepository;

class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(StatusRepository::class, StatusEloquentRepository::class);
        $this->app->bind(SituacoesRepository::class, SituacoesEloquentRepository::class);
        $this->app->bind(AtividadesRepositories::class, AtividadesEloquentRepository::class);
    }
}
