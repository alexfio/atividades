<?php

namespace Atividades\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Atividades\ViewComposers\HomeViewComposer;
use Atividades\ViewComposers\NovaAtividadeViewComposer;
use Atividades\ViewComposers\EditarAtividadeViewComposer;

class ViewComposersProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('home', HomeViewComposer::class);
        View::composer('nova_atividade', NovaAtividadeViewComposer::class);
        View::composer('editar_atividade', EditarAtividadeViewComposer::class);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
