<?php

namespace Atividades\Persistence\Eloquent\Model;

use Illuminate\Database\Eloquent\Model;
use Atividades\Persistence\Eloquent\Model\Status;
use Atividades\Persistence\Eloquent\Model\Situacao;

class Atividade extends Model
{
    public $timestamps = false;
    protected $dates = ['data_inicio', 'data_fim'];
    
    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id', 'id');
    }
    
    public function situacao()
    {
        return $this->belongsTo(Situacao::class, 'situacao_id', 'id');
    }
}
