<?php

namespace Atividades\Persistence\Eloquent\Model;

use Illuminate\Database\Eloquent\Model;
use Atividades\Persistence\Eloquent\Model\Atividade;

class Situacao extends Model
{
    protected $table = "situacoes";
    public $timestamps = false;
    
    public function atividades()
    {
        return $this->hasMany(Atividade::class, 'situacao_id', 'id');
    }
}
