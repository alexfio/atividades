<?php

namespace Atividades\Persistence\Eloquent\Model;

use Illuminate\Database\Eloquent\Model;
use Atividades\Persistence\Eloquent\Model\Atividade;

class Status extends Model
{
    protected $table = "status";
    public $timestamps = false;
   
    public function atividades()
    {
        return $this->hasMany(Atividade::class, 'status_id', 'id');
    }
}
