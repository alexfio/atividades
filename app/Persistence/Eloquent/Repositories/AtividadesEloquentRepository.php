<?php

namespace Atividades\Persistence\Eloquent\Repositories;

use Atividades\Persistence\Repositories\AtividadesRepositories;
use Atividades\Persistence\Eloquent\Model\Atividade;
use Atividades\Persistence\Eloquent\Model\Status;
use Atividades\Persistence\Eloquent\Model\Situacao;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class AtividadesEloquentRepository implements AtividadesRepositories
{
    public function update(array $dados): void
    {
        if (!isset($dados['uuid'])) {
            throw new \Exception('Atividade sem Uuid');
        }
       
        DB::transaction(function () use ($dados) {
            $atividade = Atividade::where('uuid', $dados['uuid'])->firstOrFail();
            $atividade->uuid = Uuid::uuid4()->toString();
            $atividade->nome = $dados['nome'];
            $atividade->data_inicio = $dados['inicio'];
            $atividade->data_fim = $dados['termino'];
            $atividade->descricao = $dados['descricao'];
            $status = Status::findOrFail($dados['status']);
            $situacao = Situacao::findOrFail($dados['situacao']);
            $atividade->status()->associate($status);
            $atividade->situacao()->associate($situacao);

            $atividade->save();
        });
    }

    public function save(array $dados): void
    {
        if (isset($dados['uuid'])) {
            throw new \Exception('Atividade com Uuid');
        }

        DB::transaction(function () use ($dados) {
            $atividade = new Atividade();
            $atividade->uuid = Uuid::uuid4()->toString();
            $atividade->nome = $dados['nome'];
            $atividade->data_inicio = $dados['inicio'];
            $atividade->data_fim = $dados['termino'];
            $atividade->descricao = $dados['descricao'];
            $status = Status::findOrFail($dados['status']);
            $situacao = Situacao::findOrFail($dados['situacao']);
            $atividade->status()->associate($status);
            $atividade->situacao()->associate($situacao);

            $atividade->save();
        });
    }

    public function getAll(): array
    {
        return Atividade::with(['status', 'situacao'])
                        ->orderBy('id', 'asc')
                        ->paginate(4)->toArray();
    }

    public function getByCriteria(array $criteria = []): array
    {
        if (count($criteria) > 0) {
            $atividades = Atividade::query();
            $atividades = $atividades->with(['status', 'situacao']);
            
            if (isset($criteria['status'])) {
                $atividades = $atividades->whereHas('status', function ($status) use ($criteria) {
                    $status->where('id', $criteria['status']);
                });
            }

            if (isset($criteria['situacao'])) {
                $atividades = $atividades->whereHas('situacao', function ($situacao) use ($criteria) {
                    $situacao->where('id', $criteria['situacao']);
                });
            }

            return $atividades->orderBy('id', 'asc')
                              ->paginate(1000)
                              ->toArray();
        }
        
        return $this->getAll();
    }

    public function getByUuid(string $uuid): array
    {
        return Atividade::with(['status', 'situacao'])
                        ->where('uuid', $uuid)
                        ->firstOrFail()
                        ->toArray();
    }
}
