<?php

namespace Atividades\Persistence\Eloquent\Repositories;

use Atividades\Persistence\Repositories\SituacoesRepository;
use Atividades\Persistence\Eloquent\Model\Situacao;

class SituacoesEloquentRepository implements SituacoesRepository
{
    public function getAll(): array
    {
        return Situacao::all()->toArray();
    }
}
