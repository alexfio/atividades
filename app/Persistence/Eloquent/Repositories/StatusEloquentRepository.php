<?php

namespace Atividades\Persistence\Eloquent\Repositories;

use Atividades\Persistence\Repositories\StatusRepository;
use Atividades\Persistence\Eloquent\Model\Status;

class StatusEloquentRepository implements StatusRepository
{
    public function getAll(): array
    {
        return Status::all()->toArray();
    }
}
