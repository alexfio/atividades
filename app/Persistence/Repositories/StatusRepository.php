<?php
namespace Atividades\Persistence\Repositories;

interface StatusRepository
{
    public function getAll() : array;
}
