<?php

namespace Atividades\Persistence\Repositories;

interface AtividadesRepositories
{
    public function update(array $dados) : void;
    public function save(array $dados) : void;
    public function getAll() : array ;
    public function getByCriteria(array $criteria = []) : array;
    public function getByUuid(string $uuid) : array;
}
