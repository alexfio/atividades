<?php

namespace Atividades\Persistence\Repositories;

interface SituacoesRepository
{
    public function getAll() : array;
}
