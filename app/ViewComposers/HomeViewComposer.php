<?php

namespace Atividades\ViewComposers;

use Illuminate\View\View;
use Atividades\Persistence\Repositories\StatusRepository;
use Atividades\Persistence\Repositories\SituacoesRepository;

class HomeViewComposer
{
    private $statusRepository;
    private $situacoesRepository;
    
    public function __construct(StatusRepository $statusRepository, SituacoesRepository $situacoesRepository)
    {
        $this->statusRepository = $statusRepository;
        $this->situacoesRepository = $situacoesRepository;
    }

    public function compose(View $view)
    {
        $status = $this->statusRepository->getAll();
        $situacoes = $this->situacoesRepository->getAll();
        
        $selectStatus['options'] = [];
        foreach ($status as $dado) {
            $selectStatus['options'][$dado['id']] = $dado['descricao'];
        }
        
        
        $selectSituacao['options'] = [];
        foreach ($situacoes as $dado) {
            $selectSituacao['options'][$dado['id']] = $dado['descricao'];
        }
        
       
        return $view->with('selectStatus', $selectStatus)
                    ->with('selectSituacao', $selectSituacao);
    }
}
