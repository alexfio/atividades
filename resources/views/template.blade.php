<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        @include('style')

        <title>Atividades</title>


    </head>
    <body>
        <header>
            @include('barra_superior')
        </header>
        
        @yield('conteudo')

        @section('scripts')

        <script src="{{url('lib/Jquery/dist/jquery.min.js')}}"></script>
        <script src = "{{url('js/jquery-ui/jquery-ui.min.js')}}"></script>
        <script src = "{{url('js/jquery-ui/ui/i18n/datepicker-pt-BR.js')}}"></script>
        <script src = "{{url('lib/jquery-mask-plugin/dist/jquery.mask.min.js')}}"></script>
        <script src="{{url('lib/bootstrap/dist/js/bootstrap.min.js')}}" ></script>
        <script src="{{url('lib/jquery-loading/dist/jquery.loading.min.js')}}"></script>
        <script src = "{{url('js/form_atividade.js')}}"></script>
        <script src = "{{url('js/home.js')}}"></script>

        @show

    </body>
</html>
