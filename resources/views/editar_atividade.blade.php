@extends('template')

@section('conteudo')
<div class ="container animated fadeIn">
    @include('form_atividade', ['titulo' => 'Editar Atividade', 'rota' => route('editar-atividade')])
</div>
@endsection