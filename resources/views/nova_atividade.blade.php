@extends('template')

@section('conteudo')
<div class ="container animated fadeIn">
    @if(session()->has('cadastrado_falha'))
    <div class ="row mt-3"> 
        <div class ="offset-2 col-lg-8 animated bounceIn">
            <div class="alert alert-warning alert-dismissible fade show text-black-50" role="alert">
                <i class = "fa fa-exclamation"> </i>
                {{ session('cadastrado_falha') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>
    @endif
    @include('form_atividade', ['titulo' => 'Nova Atividade', 'rota' => 'cadastrar-atividade'])
</div>
@endsection