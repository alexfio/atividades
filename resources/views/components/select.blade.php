
<select name = "{{$name}}" class = "form-control" required>
    <option value = "" disabled {{ $selected == 0 ? "selected" : "" }} >
        {{ $label }}
    </option>
    
    @foreach($select['options'] as $id => $valor) 
        <option value = "{{$id}}" 
                {{ $selected == $id ? "selected" : "" }} >
            {{$valor}}
        </option>
    @endforeach
    
</select>