@extends('template')

@section('conteudo')

@if(session()->has('msg_conclusao'))
    
    <div class ='row mt-4 animated slideInUp'>
        <div class ='offset-2  col-lg-8'>
            <div class ='alert alert-warning'>
                {{session('msg_conclusao')}}.
            </div>
        </div>
    </div>

@endif

<div class ='row mt-3 animated fadeIn'>
    <div class ='offset-2  col-lg-8'>
        <div class = 'card'>
            <div class = 'card-header bg-info text-white text-uppercase'>
                <i class = "fa fa-list"></i>
                <b>{{ $atividade['nome'] }}</b>
            </div>
        </div>
        <div class ="card-body bg-light">
            <div class ="row mt">
                <div class ="col-lg-6">
                    <label class = "text-black-50">
                        <i class ="fa fa-refresh"></i>
                        <b>Status</b>
                    </label>
                    <br>
                    <span class ="text-black-50">
                        @switch($atividade['status']['id'])
                                @case('1')
                                    @tag(['css_class' => 'danger'])
                                        Pendente 
                                    @endtag
                                @break
                                @case('2')
                                    @tag(['css_class' => 'warning'])
                                        Em Desenvolvimento 
                                    @endtag
                                @break
                                @case('3')
                                    @tag(['css_class' => 'info'])
                                        Em Teste
                                    @endtag
                                @break
                                @case('4')
                                    @tag(['css_class' => 'success'])
                                        Concluído
                                    @endtag
                                @break        
                                        
                            @endswitch
                    </span>
                </div>
                <div class ="col-lg-6">
                    <label class = "text-black-50">
                        <i class ="fa fa-trello"></i>
                        <b>Situação</b>
                    </label>
                    <br>
                    <span class ="text-black-50">
                        @switch($atividade['situacao']['id'])
                                @case('1')
                                    @tag(['css_class' => 'primary'])
                                        Ativo 
                                    @endtag
                                @break
                                @case('2')
                                    @tag(['css_class' => 'dark'])
                                        Inativo 
                                    @endtag
                                @break
                                        
                       @endswitch   
                    </span>
                </div>
            </div>
            <div class ="row mt-3">
                <div class ="col-lg-6">
                    <label class = "text-black-50">
                        <i class ="fa fa-calendar"></i>
                        <b>Data de Início</b>
                    </label>
                    <br>
                    <span class ="text-black-50">
                        {{$atividade['data_inicio']}}
                    </span>
                </div>
                <div class ="col-lg-6">
                    <label class = "text-black-50">
                        <i class ="fa fa-calendar"></i>
                        <b>Data de Fim</b>
                    </label>
                    <br>
                    <span class ="text-black-50">
                        
                        @if(!empty($atividade['data_fim']))
                            {{$atividade['data_fim']}}
                        @else
                            Não definida 
                        @endif
                    </span>
                </div>
            </div>
            <div class ="row mt-3">
                <div class ="col-lg-12">
                    <label class = "text-black-50">
                        <i class ="fa fa-list"></i>
                        <b>Descrição</b>
                    </label>
                    <br>
                    <p class ="text-black-50 text-justify">
                        {{ $atividade['descricao'] }}
                    </p>
                </div>
                
            </div>
            
        </div>
        <div class ="card-footer bg-info">
            
        </div>
    </div>
</div>

@endsection