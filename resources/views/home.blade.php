@extends('template')

@section('conteudo')

<div class ='container'>
    
    @if(session()->has('cadastrado_sucesso'))
    <div class ="row mt-3"> 
        <div class ="offset-1 col-lg-10">
            <div class="alert alert-primary alert-dismissible fade show text-black-50" role="alert">
                <i class = "fa fa-check"> </i>
                {{ session('cadastrado_sucesso') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>
    @endif
    
    <div class ='row mt-4'>
        <div class = 'offset-1  col-lg-10'>
            <div class ='card'>
                <div class = 'card-header bg-info text-white'>
                    <i class = "fa fa-search"></i>
                    <span>Filtros</span>
                </div>
                <div class = 'card-body'>
                    <div class ="row">
                        <div class ="col-lg-4">
                            @select(['label' => 'Filtrar por Status', 'select' => $selectStatus, 'name' => 'status', 'selected' => '0'])
                            @endselect
                        </div>
                        <div class ="col-lg-4">
                            @select(['label' => 'Filtrar por Situação','select' => $selectSituacao, 'name' => 'situacao', 'selected' => '0'])
                            @endselect
                        </div>
                        <div class ="col-lg-4">
                            <a class ="btn btn-info btn-block" href="/">
                                <i class = "fa fa-list"></i>
                                Listar Todos
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    @if(count($atividades['data']) > 0)
    <div id ="linha_atividades" class = "row mt-3 animated fadeIn">
        <div class = 'offset-1  col-lg-10'>
            <table id = 'tabela_atividades' class="table">
                <thead class="bg-info text-white">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Atividade</th>
                        <th scope="col">Status</th>
                        <th scope="col">Situação</th>
                        <th scope="col">Ação</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($atividades['data'] as $atividade)
                        <tr class = "text-black-50 {{ $atividade['status']['id'] == 4 ? 'table-success' : '' }} ">
                        <th scope="row"> {{$atividade['id']}} </th> 
                        <td>{{ $atividade['nome'] }}</td>
                        <td>
                            
                            @switch($atividade['status']['id'])
                                @case('1')
                                    @tag(['css_class' => 'danger'])
                                        Pendente 
                                    @endtag
                                @break
                                @case('2')
                                    @tag(['css_class' => 'warning'])
                                        Em Desenvolvimento 
                                    @endtag
                                @break
                                @case('3')
                                    @tag(['css_class' => 'info'])
                                        Em Teste
                                    @endtag
                                @break
                                @case('4')
                                    @tag(['css_class' => 'success'])
                                        Concluído
                                    @endtag
                                @break        
                                        
                            @endswitch
                          
                        </td>
                        <td>
                            @switch($atividade['situacao']['id'])
                                @case('1')
                                    @tag(['css_class' => 'primary'])
                                        Ativo 
                                    @endtag
                                @break
                                @case('2')
                                    @tag(['css_class' => 'dark'])
                                        Inativo 
                                    @endtag
                                @break
                                        
                            @endswitch    
                                
                            
                        </td>

                        <td>
                            @actionButton(['icon' => 'fa-search', 'rota' => route('ver-detalhes', $atividade['uuid'])])
                                Ver Detalhes
                            @endactionButton

                            @actionButton(['icon' => 'fa-edit', 'rota' => route('alterar-atividade', $atividade['uuid'])])
                                Editar Atividade
                            @endactionButton

                        </td>
                    </tr>
                    @endforeach
                    
                </tbody>
            </table>

        </div>
    </div>
    <div  class ="row">
        <div class = 'offset-1  col-lg-10'>
            <nav aria-label="Page navigation example">
                <ul id ="lista_paginacao" class="pagination justify-content-center">
                    @for($p = 1; $p <= ceil($atividades['total']/4); $p++)
                        <li class="page-item">
                            <a class="page-link" href="{{route('home')}}?page={{$p}}">
                               {{ $p }}
                            </a>
                        </li>
                    @endfor
                </ul>
            </nav>
        </div>
    </div>
    @else
    <div class ="row mt-3"> 
        <div class ="offset-1 col-lg-10">
            <div class="alert alert-warning animated fadeIn" role="alert">
                <i class = "fa fa-info"> </i> &nbsp;
                Nenhuma Atividade Encontrada.

            </div>
        </div>
    </div>
    @endif
    
    <div class ="row mt-2 mb-4">
        <div class = 'offset-1  col-lg-10'>
            <a class ="btn btn-info btn-block text-white" href="{{route('nova-atividade')}}">
                <i class = "fa fa-plus"></i>
                Nova Atividade
            </a>
        </div>
    </div>
</div>

@endsection