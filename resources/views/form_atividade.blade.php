
<div class ='row'>
    <div class ='col-lg-12'>
        <form id ="form_atividade" action="{{$rota}}" method="post" >
            <input type="hidden" name = "_token" value = "{{csrf_token()}}">

            @isset($atividade['uuid'])
                <input type="hidden" name = "uuid" value = "{{$atividade['uuid']}}">
            @endisset
            
            @if($errors->any())
            <div class = 'row mt-3'>
               <div class ="offset-2 col-lg-8 animated bounceIn">
                    <div class = 'alert alert-danger'>
                        <p>
                            <i class = "fa  fa-exclamation-triangle"></i>
                            Atenção
                        </p>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            @endif
            
            <div class ="row mt-3">
                <div class ="offset-2 col-lg-8">
                    <div class = "card">
                        <div class ="card-header bg-info text-white text-uppercase">
                            <i class = "fa fa-list"></i>
                            {{$titulo}}
                        </div>
                        <div class ="card-body">
                            <div class ="row">
                                <div class = "col-lg-12">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class= "fa fa-tasks" ></i>
                                            </span>
                                        </div>
                                        <input type="text" 
                                               
                                               name = 'nome'
                                               class="form-control" 
                                               placeholder="Nome" 
                                               value = "{{ $atividade['nome'] or old('nome')}}"
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div class ="row">
                                <div class = "col-lg-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class= "fa fa-calendar" ></i>
                                            </span>
                                        </div>
                                        <input type="text" 
                                               id ='inicio_em'
                                               name = "inicio"
                                               class="form-control" 
                                               placeholder="Início Em" 
                                               value = "{{$atividade['data_inicio'] or old('inicio')}}"
                                               required>
                                    </div>
                                </div>
                                <div class = "col-lg-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class= "fa fa-calendar" ></i>
                                            </span>
                                        </div>
                                        <input type="text" 
                                               id ='termino_em'
                                               name = "termino"
                                               class="form-control" 
                                               placeholder="Término Em" 
                                               value = "{{$atividade['data_fim'] or old('termino')}}">
                                    </div>
                                </div>
                            </div>
                            <div class ="row">
                                <div class = "col-lg-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class= "fa fa-refresh" ></i>
                                            </span>
                                        </div>
                                        @select(['label' => 'Status', 'select' => $selectStatus, 'name' => 'status', 'selected' =>  $atividade['status']['id'] ?? old('status') ?? '0'])
                                        @endselect
                                    </div>
                                </div>
                                <div class = "col-lg-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class= "fa fa-trello" ></i>
                                            </span>
                                        </div>
                                        @select(['label' => 'Situação','select' => $selectSituacao, 'name' => 'situacao', 'selected' => $atividade['situacao']['id'] ?? old('situacao') ?? '0'])
                                        @endselect
                                    </div>
                                </div>
                            </div>
                            <div class ="row">
                                <div class = "col-lg-12">
                                    <textarea class = "form-control" 
                                              name = "descricao"
                                              placeholder="Descrição" 
                                              required 
                                              maxlength = "600"
                                              rows = "8" cols = "40">{{ $atividade['descricao'] or old('descricao')}}</textarea>
                                </div>
                            </div>
                            <div class ="row mt-3">
                                <div class ="col-lg-12">
                                    <button type ="submit" id = "btn_submit" class = "btn btn-info btn-block">
                                        <i class ="fa fa-save"></i>
                                        Salvar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
